Fluent-bit examples by amazon:
https://github.com/aws-samples/amazon-ecs-firelens-examples/tree/mainline/examples/fluent-bit

Cloudwatch plugin for fluent bit:
https://github.com/aws/amazon-cloudwatch-logs-for-fluent-bit

NOTE: there is better optimized written in C plugin called cloudwatch_logs, but it lacks some functionality of current one (e.g. stream name templating).

Fluent bit is log aggregator and parser that has multiple plugins e.g. firehose, cloudwatch, datadog etc. (https://docs.fluentbit.io/manual/pipeline/outputs).


Cloudwatch plugin configuration notes:
- Do not use `log_stream_prefix`. It's not compatibile with `log_stream_name`.
- `log_key` set to `log` is crucial, otherwise instead of simple log line there will be ECS metadata added to log event like this
```
{
	"log": "log line",
	"ecs_task_arn": XXXXXXXXX,
	"ecs_container_id": XXXXXXXXXXXX,
}
```

DataDog plugin configuration:
- Due to configuration change from ecs task to embedded config file logs no longer contain tags like container_id, ecs_task_id. Instead those values are represented as event attributes - filtering is available using syntax: @ecs_task_id:value.
