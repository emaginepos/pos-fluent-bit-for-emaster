#!/bin/bash

#
# First use this to log in to aws:
# aws ecr get-login-password --region us-east-1 --profile emagine-qa-full | docker login --username AWS --password-stdin 690070306181.dkr.ecr.us-east-1.amazonaws.com
# 
# Then eedit version below.
# Finally, execute this script to build and push QA and PROD image of fluentbit for emaster.
#

# edit version
QA_VERSION="qa-20200415l"
PROD_VERSION="prod-20200415l"

# do not edit this variables
IMAGE_NAME="690070306181.dkr.ecr.us-east-1.amazonaws.com/emaginepos/fluentbit-emaster"
QA_NAME="$IMAGE_NAME:$QA_VERSION"
PROD_NAME="$IMAGE_NAME:$PROD_VERSION"

# build and push QA image
docker build -f Dockerfile-qa -t $QA_NAME .
docker push $QA_NAME

# build and push PROD image
docker build -f Dockerfile-prod -t $PROD_NAME .
docker push $PROD_NAME
